/*
 * 本源代码知识产权属于北京新叶至恒软件技术有限公司（NLTE）所有。
 * <P>
 * 本项目以Apache License Version 2.0协议发布，鼓励代码共享和尊重原作者的著作权，
 * <P>
 * 同样允许代码修改，再发布（作为开源或商业软件）。
 * <P>
 * 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，
 * <P>
 * 商标，不得以任何形式删去“北京新叶至恒软件技术有限公司（NLTE）”的版权声明。
 * <P>
 * 北京新叶至恒软件技术有限公司官方网址：
 * <P>
 *
 *     http://www.nlte.cn
 *
 * <P>
 * NLTE主要服务于石油、化工、核电、建筑等传统企业，致力于传统行业的现代科技化转型，
 * <P>
 * 提供软件培训、软件研发（包括二次研发）、研发咨询、系统架构、高性能计算集群（HPC）搭建等一体化的专业服务。
 * <P>
 * 公司以“专于行业应用，用心铸造永恒”为核心价值。
 */
package cn.nlte.science.units.area;

import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.empty.UEmpty;
import cn.nlte.science.units.length.ULength_in;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author yetao
 */
public class UAreaTest {
    
    public UAreaTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UArea_m2());
        data = data.convertTo(new UArea_in2());
        assertEquals(Math.pow(1/25.4E-3,2), data.getValue(),1E-10);
    }

    @Test
    public void testAdd() {
        ValueWithUnit data1 = new ValueWithUnit(1, new UArea_m2());
        ValueWithUnit data2 = new ValueWithUnit(1, new UArea_in2());
        ValueWithUnit data = data1.add(data2).convertTo(new UArea_in2());
        assertEquals(Math.pow(1/25.4E-3,2)+1.0, data.getValue(),1E-10);
    }

    @Test
    public void testSub() {
        ValueWithUnit data1 = new ValueWithUnit(1, new UArea_m2());
        ValueWithUnit data2 = new ValueWithUnit(1, new UArea_in2());
        ValueWithUnit data = data1.sub(data2).convertTo(new UArea_in2());
        assertEquals(Math.pow(1/25.4E-3,2)-1.0, data.getValue(),1E-10);
    }

    @Test
    public void testMultiply() {
        ValueWithUnit data1 = new ValueWithUnit(1, new ULength_in());
        ValueWithUnit data2 = new ValueWithUnit(1, new ULength_in());
        ValueWithUnit data = data1.multiply(data2).convertTo(new UArea_in2());
        assertEquals(Math.pow(1.0,2), data.getValue(),1E-10);
    }

    @Test
    public void testDivide() {
        ValueWithUnit data1 = new ValueWithUnit(1, new UArea_m2());
        ValueWithUnit data2 = new ValueWithUnit(1, new UArea_in2());
        ValueWithUnit data = data1.divide(data2).convertTo(new UEmpty());
        assertEquals(Math.pow(1/25.4E-3,2), data.getValue(),1E-10);
    }
    
}
