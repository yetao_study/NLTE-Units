/*
 * 本源代码知识产权属于北京新叶至恒软件技术有限公司（NLTE）所有。
 * <P>
 * 本项目以Apache License Version 2.0协议发布，鼓励代码共享和尊重原作者的著作权，
 * <P>
 * 同样允许代码修改，再发布（作为开源或商业软件）。
 * <P>
 * 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，
 * <P>
 * 商标，不得以任何形式删去“北京新叶至恒软件技术有限公司（NLTE）”的版权声明。
 * <P>
 * 北京新叶至恒软件技术有限公司官方网址：
 * <P>
 *
 *     http://www.nlte.cn
 *
 * <P>
 * NLTE主要服务于石油、化工、核电、建筑等传统企业，致力于传统行业的现代科技化转型，
 * <P>
 * 提供软件培训、软件研发（包括二次研发）、研发咨询、系统架构、高性能计算集群（HPC）搭建等一体化的专业服务。
 * <P>
 * 公司以“专于行业应用，用心铸造永恒”为核心价值。
 */
package cn.nlte.science.units.pressure;

import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.area.UArea_mm2;
import cn.nlte.science.units.force.UForce_N;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UPressureTest {
    
    public UPressureTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UPressure_KPa());
        assertEquals(1, data.getValue(),1E-10);
        data = data.convertTo(new UPressure_Pa());
        assertEquals(1000, data.getValue(),1E-10);
        data = data.convertTo(new UPressure_MPa());
        assertEquals(0.001, data.getValue(),1E-10);
        data = data.convertTo(new UPressure_bar());
        assertEquals(0.01, data.getValue(),1E-10);
        data = data.convertTo(new UPressure_HPa());
        assertEquals(10, data.getValue(),1E-7);
        data = data.convertTo(new UPressure_mbar());
        assertEquals(10, data.getValue(),1E-7);
        data = data.convertTo(new UPressure_atm());
        assertEquals(0.00986923, data.getValue(),1E-6);
        data = data.convertTo(new UPressure_mmHg());
        assertEquals(7.50061683, data.getValue(),1E-6);
        data = data.convertTo(new UPressure_lbfPerft2());
        assertEquals(20.88543512, data.getValue(),1E-6);
        data = data.convertTo(new UPressure_lbfPerin2());
        assertEquals(0.14503774, data.getValue(),1E-6);
        data = data.convertTo(new UPressure_KgfPerm2());
        assertEquals(101.9716213, data.getValue(),1E-6);
    }

    @Test
    public void testAdd() {
        
    }

    @Test
    public void testSub() {
        
    }

    @Test
    public void testMultiply() {
        
    }

    @Test
    public void testDivide() {
        ValueWithUnit force = new ValueWithUnit(1.2, new UForce_N());
        ValueWithUnit area = new ValueWithUnit(1.0, new UArea_mm2());
        ValueWithUnit pressure = force.divide(area).convertTo(new UPressure_MPa());
        assertEquals(1.2, pressure.getValue(), 1E-6);
    }
    
}
